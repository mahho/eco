# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0004_auto_20141005_2049'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building',
            name='sector',
            field=models.ForeignKey(related_name='backend.building', to='backend.Sector'),
        ),
    ]
