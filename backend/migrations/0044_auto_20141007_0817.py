# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0043_auto_20141007_0810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='warehouseresource',
            name='sector',
            field=models.ForeignKey(related_name='warehouse_resources', to='backend.Sector', null=True),
        ),
    ]
