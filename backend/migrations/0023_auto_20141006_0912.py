# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0022_buildingproperty_building'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sectorbuilding',
            name='sector',
            field=models.ForeignKey(related_name='sectorBuildings', to='backend.Sector'),
        ),
    ]
