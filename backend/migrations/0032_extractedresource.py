# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0031_auto_20141006_1237'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExtractedResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('resource_name', models.ForeignKey(to='backend.ResourceName', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
