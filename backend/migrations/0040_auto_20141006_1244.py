# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0039_sector_extracted_resources'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sector',
            name='extracted_resources',
        ),
        migrations.AddField(
            model_name='extractedresource',
            name='sector',
            field=models.ForeignKey(related_name='extracted_resources', to='backend.Sector', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sectorresource',
            name='resource_name',
            field=models.ForeignKey(related_name='sector_resources', to='backend.ResourceName'),
        ),
    ]
