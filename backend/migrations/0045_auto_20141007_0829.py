# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0044_auto_20141007_0817'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sectorbuilding',
            name='sector',
            field=models.ForeignKey(related_name='sector_buildings', to='backend.Sector'),
        ),
    ]
