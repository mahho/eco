# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0041_auto_20141006_1922'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buildingproperty',
            name='produces_amount',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='buildingproperty',
            name='requires_amount',
            field=models.IntegerField(default=0),
        ),
    ]
