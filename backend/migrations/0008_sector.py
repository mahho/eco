# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('backend', '0007_auto_20141006_0807'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plains', models.IntegerField(default=50)),
                ('hills', models.IntegerField(default=50)),
                ('position_x', models.IntegerField(null=True)),
                ('position_y', models.IntegerField(null=True)),
                ('owner', models.ForeignKey(related_name='sectors', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
