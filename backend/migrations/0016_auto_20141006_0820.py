# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0015_resourcetype_amount'),
    ]

    operations = [
        migrations.RenameField(
            model_name='resourcetype',
            old_name='amount',
            new_name='production_amount',
        ),
    ]
