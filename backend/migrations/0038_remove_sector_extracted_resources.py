# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0037_sector_extracted_resources'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sector',
            name='extracted_resources',
        ),
    ]
