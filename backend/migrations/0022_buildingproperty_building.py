# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0021_remove_resourcetype_production_amount'),
    ]

    operations = [
        migrations.AddField(
            model_name='buildingproperty',
            name='building',
            field=models.ForeignKey(related_name='building', to='backend.Building', null=True),
            preserve_default=True,
        ),
    ]
