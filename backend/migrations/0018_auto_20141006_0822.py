# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0017_auto_20141006_0822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buildingtype',
            name='production_speed',
            field=models.IntegerField(default=1),
        ),
    ]
