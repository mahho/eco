# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0042_auto_20141007_0708'),
    ]

    operations = [
        migrations.CreateModel(
            name='WarehouseResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('resource_name', models.ForeignKey(to='backend.ResourceName', null=True)),
                ('sector', models.ForeignKey(related_name='extracted_resources', to='backend.Sector', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='extractedresource',
            name='resource_name',
        ),
        migrations.RemoveField(
            model_name='extractedresource',
            name='sector',
        ),
        migrations.DeleteModel(
            name='ExtractedResource',
        ),
    ]
