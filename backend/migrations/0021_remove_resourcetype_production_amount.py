# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0020_auto_20141006_0853'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resourcetype',
            name='production_amount',
        ),
    ]
