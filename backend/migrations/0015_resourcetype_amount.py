# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0014_warehouseresource'),
    ]

    operations = [
        migrations.AddField(
            model_name='resourcetype',
            name='amount',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
