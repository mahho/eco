# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0019_auto_20141006_0832'),
    ]

    operations = [
        migrations.AddField(
            model_name='buildingproperty',
            name='produces_amount',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingproperty',
            name='requires_amount',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
