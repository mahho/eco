# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0009_resourcetype'),
    ]

    operations = [
        migrations.CreateModel(
            name='SectorResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('resource_name', models.ForeignKey(to='backend.ResourceType')),
                ('sector', models.ForeignKey(to='backend.Sector', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
