# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0029_auto_20141006_1219'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='extractedresource',
            name='sector',
        ),
        migrations.AddField(
            model_name='sector',
            name='extracted_resources',
            field=models.OneToOneField(null=True, to='backend.ExtractedResource'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='extractedresource',
            name='resource_name',
            field=models.ForeignKey(to='backend.ResourceName', null=True),
        ),
    ]
