# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0006_auto_20141005_2050'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='building',
            name='building_name',
        ),
        migrations.RemoveField(
            model_name='building',
            name='sector',
        ),
        migrations.DeleteModel(
            name='Building',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_1',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_2',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_3',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_4',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_5',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_1',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_2',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_3',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_4',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_5',
        ),
        migrations.DeleteModel(
            name='BuildingType',
        ),
        migrations.RemoveField(
            model_name='sector',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='sectorresource',
            name='resource_name',
        ),
        migrations.RemoveField(
            model_name='sectorresource',
            name='sector',
        ),
        migrations.DeleteModel(
            name='SectorResource',
        ),
        migrations.RemoveField(
            model_name='warehouse',
            name='sector',
        ),
        migrations.DeleteModel(
            name='Sector',
        ),
        migrations.RemoveField(
            model_name='warehouseresource',
            name='name',
        ),
        migrations.DeleteModel(
            name='ResourceType',
        ),
        migrations.RemoveField(
            model_name='warehouseresource',
            name='warehouse',
        ),
        migrations.DeleteModel(
            name='Warehouse',
        ),
        migrations.DeleteModel(
            name='WarehouseResource',
        ),
    ]
