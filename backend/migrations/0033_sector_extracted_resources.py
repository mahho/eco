# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0032_extractedresource'),
    ]

    operations = [
        migrations.AddField(
            model_name='sector',
            name='extracted_resources',
            field=models.OneToOneField(null=True, to='backend.ExtractedResource'),
            preserve_default=True,
        ),
    ]
