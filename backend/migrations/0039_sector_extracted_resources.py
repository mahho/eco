# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0038_remove_sector_extracted_resources'),
    ]

    operations = [
        migrations.AddField(
            model_name='sector',
            name='extracted_resources',
            field=models.ForeignKey(to='backend.ExtractedResource', null=True),
            preserve_default=True,
        ),
    ]
