# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0016_auto_20141006_0820'),
    ]

    operations = [
        migrations.RenameField(
            model_name='buildingtype',
            old_name='max_level',
            new_name='size_per_level',
        ),
    ]
