# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0035_auto_20141006_1241'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sector',
            name='extracted_resources',
        ),
    ]
