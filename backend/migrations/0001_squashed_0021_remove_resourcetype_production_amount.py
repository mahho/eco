# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    replaces = [('backend', '0001_initial'), ('backend', '0002_auto_20141005_2042'), ('backend', '0003_auto_20141005_2047'), ('backend', '0004_auto_20141005_2049'), ('backend', '0005_auto_20141005_2049'), ('backend', '0006_auto_20141005_2050'), ('backend', '0007_auto_20141006_0807'), ('backend', '0008_sector'), ('backend', '0009_resourcetype'), ('backend', '0010_sectorresource'), ('backend', '0011_buildingtype'), ('backend', '0012_building'), ('backend', '0013_warehouse'), ('backend', '0014_warehouseresource'), ('backend', '0015_resourcetype_amount'), ('backend', '0016_auto_20141006_0820'), ('backend', '0017_auto_20141006_0822'), ('backend', '0018_auto_20141006_0822'), ('backend', '0019_auto_20141006_0832'), ('backend', '0020_auto_20141006_0853'), ('backend', '0021_remove_resourcetype_production_amount')]

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plains', models.IntegerField(default=50)),
                ('hills', models.IntegerField(default=50)),
                ('position_x', models.IntegerField(null=True)),
                ('position_y', models.IntegerField(null=True)),
                ('owner', models.ForeignKey(related_name='sectors', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ResourceType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SectorResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('resource_name', models.ForeignKey(to='backend.ResourceType')),
                ('sector', models.ForeignKey(to='backend.Sector', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BuildingType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('max_level', models.IntegerField()),
                ('production_speed', models.IntegerField(default=0)),
                ('produces_1', models.ForeignKey(related_name='produces_1', blank=True, to='backend.ResourceType', null=True)),
                ('produces_2', models.ForeignKey(related_name='produces_2', blank=True, to='backend.ResourceType', null=True)),
                ('produces_3', models.ForeignKey(related_name='produces_3', blank=True, to='backend.ResourceType', null=True)),
                ('produces_4', models.ForeignKey(related_name='produces_4', blank=True, to='backend.ResourceType', null=True)),
                ('produces_5', models.ForeignKey(related_name='produces_5', blank=True, to='backend.ResourceType', null=True)),
                ('requires_1', models.ForeignKey(related_name='requires_1', blank=True, to='backend.ResourceType', null=True)),
                ('requires_2', models.ForeignKey(related_name='requires_2', blank=True, to='backend.ResourceType', null=True)),
                ('requires_3', models.ForeignKey(related_name='requires_3', blank=True, to='backend.ResourceType', null=True)),
                ('requires_4', models.ForeignKey(related_name='requires_4', blank=True, to='backend.ResourceType', null=True)),
                ('requires_5', models.ForeignKey(related_name='requires_5', blank=True, to='backend.ResourceType', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.IntegerField()),
                ('building_name', models.ForeignKey(to='backend.BuildingType')),
                ('sector', models.ForeignKey(to='backend.Sector')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Warehouse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sector', models.ForeignKey(to='backend.Sector')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WarehouseResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('name', models.ForeignKey(to='backend.ResourceType')),
                ('warehouse', models.ForeignKey(to='backend.Warehouse', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='resourcetype',
            name='production_amount',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
        migrations.RenameField(
            model_name='buildingtype',
            old_name='max_level',
            new_name='size_per_level',
        ),
        migrations.AlterField(
            model_name='buildingtype',
            name='production_speed',
            field=models.IntegerField(default=1),
        ),
        migrations.CreateModel(
            name='BuildingProperty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('produces', models.ForeignKey(related_name='produces', blank=True, to='backend.ResourceType', null=True)),
                ('requires', models.ForeignKey(related_name='requires', blank=True, to='backend.ResourceType', null=True)),
                ('produces_amount', models.IntegerField(default=1)),
                ('requires_amount', models.IntegerField(default=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SectorBuilding',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.IntegerField()),
                ('building', models.ForeignKey(to='backend.Building')),
                ('sector', models.ForeignKey(to='backend.Sector')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_1',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_2',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_3',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_4',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_5',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_1',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_2',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_3',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_4',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_5',
        ),
        migrations.RenameField(
            model_name='building',
            old_name='level',
            new_name='size_per_level',
        ),
        migrations.RemoveField(
            model_name='building',
            name='building_name',
        ),
        migrations.DeleteModel(
            name='BuildingType',
        ),
        migrations.RemoveField(
            model_name='building',
            name='sector',
        ),
        migrations.AddField(
            model_name='building',
            name='name',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='building',
            name='production_speed',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='resourcetype',
            name='production_amount',
        ),
    ]
