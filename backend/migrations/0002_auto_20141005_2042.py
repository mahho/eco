# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.IntegerField()),
                ('building_name', models.ForeignKey(to='backend.BuildingType')),
                ('sector', models.ForeignKey(related_name='building', to='backend.Sector')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SectorResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('resource_name', models.ForeignKey(to='backend.ResourceType')),
                ('sector', models.ForeignKey(to='backend.Sector', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Warehouse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sector', models.ForeignKey(to='backend.Sector')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WarehouseResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('name', models.ForeignKey(to='backend.ResourceType')),
                ('warehouse', models.ForeignKey(to='backend.Warehouse', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='sectorbuildings',
            name='building_name',
        ),
        migrations.DeleteModel(
            name='SectorBuildings',
        ),
        migrations.RemoveField(
            model_name='sectorresources',
            name='resource_name',
        ),
        migrations.RemoveField(
            model_name='sector',
            name='sector_resources',
        ),
        migrations.DeleteModel(
            name='SectorResources',
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='produces_1',
            field=models.ForeignKey(related_name='produces_1', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='produces_2',
            field=models.ForeignKey(related_name='produces_2', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='produces_3',
            field=models.ForeignKey(related_name='produces_3', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='produces_4',
            field=models.ForeignKey(related_name='produces_4', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='produces_5',
            field=models.ForeignKey(related_name='produces_5', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='production_speed',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='requires_1',
            field=models.ForeignKey(related_name='requires_1', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='requires_2',
            field=models.ForeignKey(related_name='requires_2', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='requires_3',
            field=models.ForeignKey(related_name='requires_3', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='requires_4',
            field=models.ForeignKey(related_name='requires_4', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildingtype',
            name='requires_5',
            field=models.ForeignKey(related_name='requires_5', blank=True, to='backend.ResourceType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sector',
            name='hills',
            field=models.IntegerField(default=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sector',
            name='plains',
            field=models.IntegerField(default=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sector',
            name='position_x',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sector',
            name='position_y',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='resourcetype',
            name='name',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='sector',
            name='owner',
            field=models.ForeignKey(related_name='sectors', to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
