# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0018_auto_20141006_0822'),
    ]

    operations = [
        migrations.CreateModel(
            name='BuildingProperty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('produces', models.ForeignKey(related_name='produces', blank=True, to='backend.ResourceType', null=True)),
                ('requires', models.ForeignKey(related_name='requires', blank=True, to='backend.ResourceType', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SectorBuilding',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.IntegerField()),
                ('building', models.ForeignKey(to='backend.Building')),
                ('sector', models.ForeignKey(to='backend.Sector')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_1',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_2',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_3',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_4',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='produces_5',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_1',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_2',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_3',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_4',
        ),
        migrations.RemoveField(
            model_name='buildingtype',
            name='requires_5',
        ),
        migrations.RenameField(
            model_name='building',
            old_name='level',
            new_name='size_per_level',
        ),
        migrations.RemoveField(
            model_name='building',
            name='building_name',
        ),
        migrations.DeleteModel(
            name='BuildingType',
        ),
        migrations.RemoveField(
            model_name='building',
            name='sector',
        ),
        migrations.AddField(
            model_name='building',
            name='name',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='building',
            name='production_speed',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
