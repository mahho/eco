# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0040_auto_20141006_1244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buildingproperty',
            name='building',
            field=models.ForeignKey(related_name='building_property', to='backend.Building', null=True),
        ),
    ]
