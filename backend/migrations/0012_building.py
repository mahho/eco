# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0011_buildingtype'),
    ]

    operations = [
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.IntegerField()),
                ('building_name', models.ForeignKey(to='backend.BuildingType')),
                ('sector', models.ForeignKey(to='backend.Sector')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
