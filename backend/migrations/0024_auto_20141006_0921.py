# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0023_auto_20141006_0912'),
    ]

    operations = [
        migrations.AddField(
            model_name='sectorbuilding',
            name='current_production_cycle',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sectorbuilding',
            name='level',
            field=models.IntegerField(default=1),
        ),
    ]
