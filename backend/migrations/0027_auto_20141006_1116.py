# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0026_auto_20141006_1054'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ResourceType',
            new_name='ResourceName',
        ),
        migrations.RemoveField(
            model_name='warehouseresource',
            name='name',
        ),
        migrations.AddField(
            model_name='warehouseresource',
            name='resource_name',
            field=models.ForeignKey(related_name='resourceName', to='backend.ResourceName', null=True),
            preserve_default=True,
        ),
    ]
