# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0024_auto_20141006_0921'),
    ]

    operations = [
        migrations.AlterField(
            model_name='warehouse',
            name='sector',
            field=models.ForeignKey(related_name='warehouse', to='backend.Sector'),
        ),
        migrations.AlterField(
            model_name='warehouseresource',
            name='name',
            field=models.ForeignKey(related_name='resources', to='backend.ResourceType'),
        ),
    ]
