# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0033_sector_extracted_resources'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sector',
            name='extracted_resources',
            field=models.ForeignKey(to='backend.ExtractedResource', null=True),
        ),
    ]
