# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0028_auto_20141006_1217'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExtractedResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('resource_name', models.ForeignKey(related_name='resourceName', to='backend.ResourceName', null=True)),
                ('sector', models.ForeignKey(related_name='extractedResources', to='backend.Sector', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='warehouse',
            name='sector',
        ),
        migrations.RemoveField(
            model_name='warehouseresource',
            name='resource_name',
        ),
        migrations.RemoveField(
            model_name='warehouseresource',
            name='warehouse',
        ),
        migrations.DeleteModel(
            name='Warehouse',
        ),
        migrations.DeleteModel(
            name='WarehouseResource',
        ),
    ]
