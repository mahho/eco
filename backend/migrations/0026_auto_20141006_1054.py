# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0025_auto_20141006_1051'),
    ]

    operations = [
        migrations.AlterField(
            model_name='warehouseresource',
            name='name',
            field=models.ForeignKey(to='backend.ResourceType'),
        ),
        migrations.AlterField(
            model_name='warehouseresource',
            name='warehouse',
            field=models.ForeignKey(related_name='resources', to='backend.Warehouse', null=True),
        ),
    ]
