# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0027_auto_20141006_1116'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='warehouse',
            name='id',
        ),
        migrations.AlterField(
            model_name='warehouse',
            name='sector',
            field=models.OneToOneField(related_name='warehouse', primary_key=True, serialize=False, to='backend.Sector'),
        ),
    ]
