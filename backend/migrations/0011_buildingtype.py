# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0010_sectorresource'),
    ]

    operations = [
        migrations.CreateModel(
            name='BuildingType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('max_level', models.IntegerField()),
                ('production_speed', models.IntegerField(default=0)),
                ('produces_1', models.ForeignKey(related_name='produces_1', blank=True, to='backend.ResourceType', null=True)),
                ('produces_2', models.ForeignKey(related_name='produces_2', blank=True, to='backend.ResourceType', null=True)),
                ('produces_3', models.ForeignKey(related_name='produces_3', blank=True, to='backend.ResourceType', null=True)),
                ('produces_4', models.ForeignKey(related_name='produces_4', blank=True, to='backend.ResourceType', null=True)),
                ('produces_5', models.ForeignKey(related_name='produces_5', blank=True, to='backend.ResourceType', null=True)),
                ('requires_1', models.ForeignKey(related_name='requires_1', blank=True, to='backend.ResourceType', null=True)),
                ('requires_2', models.ForeignKey(related_name='requires_2', blank=True, to='backend.ResourceType', null=True)),
                ('requires_3', models.ForeignKey(related_name='requires_3', blank=True, to='backend.ResourceType', null=True)),
                ('requires_4', models.ForeignKey(related_name='requires_4', blank=True, to='backend.ResourceType', null=True)),
                ('requires_5', models.ForeignKey(related_name='requires_5', blank=True, to='backend.ResourceType', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
