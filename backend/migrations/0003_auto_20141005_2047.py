# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0002_auto_20141005_2042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building',
            name='sector',
            field=models.ForeignKey(related_name='buildings', to='backend.Sector'),
        ),
    ]
