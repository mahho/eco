from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class ResourceName(models.Model):
    name = models.CharField(max_length = 50)
    def __str__(self):
        return self.name

class Sector(models.Model):
    owner = models.ForeignKey(User, null = True, related_name = 'sectors')
    # terrain = models.ForeignKey(TerrainType, null = True)
    plains = models.IntegerField(default = 50)
    hills = models.IntegerField(default = 50)
    position_x = models.IntegerField(null = True)
    position_y = models.IntegerField(null = True)
    def __str__(self):
        return "{0}/{1} - {2}".format(self.position_x, self.position_y, self.owner)

class WarehouseResource(models.Model):
    resource_name = models.ForeignKey(ResourceName, null = True)
    amount = models.IntegerField()
    sector = models.ForeignKey(Sector, null = True, related_name = 'warehouse_resources')
    def __str__(self):
        return "[{0}] {1}: {2}".format(self.sector, self.resource_name, self.amount)

class Building(models.Model):
    name = models.CharField(max_length = 50, null = True)
    size_per_level = models.IntegerField()
    production_speed = models.IntegerField(default = 1)
    def __str__(self):
        return self.name

class BuildingProperty(models.Model):
    building = models.ForeignKey(Building, null = True, related_name = 'building_property')
    requires = models.ForeignKey(ResourceName, related_name = 'requires', null = True, blank = True)
    requires_amount = models.IntegerField(default = 0)
    produces = models.ForeignKey(ResourceName, related_name = 'produces', null = True, blank = True)
    produces_amount = models.IntegerField(default = 0)
    def __str__(self):
        return "[{0}] Requires: {1} {2} --- Produces: {3} {4}".format(
            self.building, self.requires_amount, self.requires, self.produces_amount, self.produces)

class SectorResource(models.Model):
    sector = models.ForeignKey(Sector, null = True)
    resource_name = models.ForeignKey(ResourceName, related_name = 'sector_resources')
    amount = models.IntegerField()
    def __str__(self):
        return "[{0}] {1}: {2} left".format(self.sector, self.resource_name, self.amount)

class SectorBuilding(models.Model):
    sector = models.ForeignKey(Sector, related_name = 'sector_buildings')
    building = models.ForeignKey(Building)
    level = models.IntegerField(default = 1)
    current_production_cycle = models.IntegerField(default = 0)
    def __str__(self):
        return "[{0}] {1}: {2} level".format(self.sector, self.building, self.level)

