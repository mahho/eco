from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from backend.models import *
from noise import pnoise2
import random

class Command(NoArgsCommand):
    help = 'Map generator'

    def handle(self, *args, **options):
        width = 50;
        height = 50;

        if Sector.objects.count() == 0:
            print("Generating new map")
            randomx = random.uniform(0, 1024)
            randomy = random.uniform(0, 1024)
            for y in range(0, height):
                print ('row:', y)
                for x in range(0, width):
                    data = pnoise2((x + randomx) * 0.05, (y + randomy) * 0.05, 16)
                    modifier = data * 100
                    # print(modifier)
                    sector = Sector()
                    sector.position_x = x
                    sector.position_y = y
                    sector.plains = 50 + modifier
                    sector.hills = 100 - int(sector.plains)
                    sector.save()

                    ironResource = SectorResource()
                    ironResource.resource_name = ResourceName.objects.get(name="Iron Ore")
                    ironResource.amount = random.randint(1, 5000)
                    ironResource.sector = sector
                    ironResource.save()

                    coalResource = SectorResource()
                    coalResource.resource_name = ResourceName.objects.get(name="Coal")
                    coalResource.amount = random.randint(1, 10000)
                    coalResource.sector = sector
                    coalResource.save()

                    goldResource = SectorResource()
                    goldResource.resource_name = ResourceName.objects.get(name="Gold Ore")
                    goldResource.amount = random.randint(1, 1000)
                    goldResource.sector = sector
                    goldResource.save()
        else:
            print('Map already exists')
           
        print('Successfully')
