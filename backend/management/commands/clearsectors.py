from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from backend.models import *

class Command(NoArgsCommand):
    help = 'Clearing sectors'

    def handle(self, *args, **options):
        sectors = Sector.objects.all().delete()
        print("Successfully")
