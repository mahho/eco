from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'eco.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'frontend.views.index', name='index'),
    url(r'^intro/$', 'django.contrib.auth.views.login'),
    url(r'^sector/(\d+)$', 'frontend.views.sector_show', name='sector_show'),
    url(r'^sector/settle/(\d+)$', 'frontend.views.sector_settle', name='sector_settle'),
    url(r'^logout/$', 'frontend.views.user_logout', name='logout'),
)

