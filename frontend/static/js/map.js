var map = [
    rawdata = null,
    tiles = [],
];

function Tile(x, y, plains, hills) {
    var tile = [];
    tile.position_x = x;
    tile.position_y = y
    tile.plains = plains;
    tile.hills = hills;
    return tile;
}

map.create = function(mapdata) {
    // console.log("generating map");
    // console.log(mapdata);
    var rawdata = JSON.parse("[" + mapdata + "]");
    for (var x = 0; x < 50; x++) {
        tiles[x] = [];
    }
    rawdata.forEach(function (data) {
        tiles[data[0]][data[1]] = new Tile(data[0], data[1], data[2], data[3]);
        // tiles.push(tile);
    });
}

map.draw = function(from, to) {
    // console.log("drawing map");
    var canvas = document.getElementById("map");
    var ctx = canvas.getContext("2d");
    // ctx.beginPath();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle ="#aaaaaa";
    ctx.fill();
    ctx.fillRect(0,0,canvas.width, canvas.height);

    for (var x = from; x < from + 25; x++) {
        for (var y = to; y < to + 25; y++) {
            var id = parseInt(tiles[x][y].plains/10);
            var image = new Image();
            image.src = "/static/images/map/tile" + id + ".png";
            ctx.drawImage(image, (x - from) * 20, (y - to) * 20);
        }
    }
    ctx.restore();

    // tiles.forEach(function (tile) {
    //     var image = new Image();
    //     var id = parseInt(tile.plains/10);
    //     // console.log(id);
    //     image.src = "/static/images/map/tile" + id + ".png";
    //     ctx.drawImage(image, tile.position_x * 20, tile.position_y * 20)
    //     // console.log(image.src);
    // });
   
}