from django.contrib import admin
from backend.models import *
# Register your models here.

admin.site.register(Sector)
admin.site.register(ResourceName)
admin.site.register(SectorResource)
admin.site.register(BuildingProperty)
admin.site.register(Building)
admin.site.register(SectorBuilding)
admin.site.register(WarehouseResource)
