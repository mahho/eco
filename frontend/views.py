from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from backend.models import * 
import json
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout, get_user
from django.core.exceptions import ObjectDoesNotExist
from pprint import pprint

# Create your views here.
@login_required
def index(request):
    sectorsData = Sector.objects.all()
    sectorsArray = []
    user = request.user

    for sector in sectorsData:
        sectorArray = [sector.position_x, sector.position_y, sector.plains, sector.hills]
        sectorsArray.append(json.dumps(sectorArray))

    return render(request, 'index.html', {'mapdata': json.dumps(sectorsArray)})

@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/intro/')

@login_required
def sector_show(request, id):
    try:
        sector = Sector.objects.get(pk=int(id))
        return render(request, 'sector.html', {'sector': sector})
    except ObjectDoesNotExist:
        return HttpResponse("sector not found")

@login_required
def sector_settle(request, id):
    try: 
        sector = Sector.objects.get(id=int(id))
        if sector.owner == None:
            sector.owner = User.objects.get(username=get_user(request))
            sector.save()
            for resourceName in ResourceName.objects.all():
                warehouse_resource = WarehouseResource()
                warehouse_resource.resource_name = resourceName
                warehouse_resource.sector = sector
                warehouse_resource.amount = 0
                warehouse_resource.save()
            return HttpResponseRedirect('/sector/' + id)
        else:
            return HttpResponse("Sector already colinizated")
    except ObjectDoesNotExist:
        return HttpResponse("Sector not found")


